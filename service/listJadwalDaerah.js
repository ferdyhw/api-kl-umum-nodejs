
const connection = require('../config');
require('dotenv').config();
// const NodeCache = require('node-cache');

const {
    DB_HOST_DBLINK_MASTER
} = process.env;

// const cache = new NodeCache();
const listJadwalDaerah = async (req, res) => {
    try {
        const { tahun, idDaerah } = req.body;

        const daerah = req.body.kodeProp;
        connection.connect(daerah);

        const listJadwal = `SELECT 
        jdw.id_daerah, 
        np.id_prop,
        np.nama_daerah as nama_prop,
        nd.nama_daerah,
        jdw.id_jadwal,
        jdw.tahun,
        jdw.id_tahap,
        thp.nama_tahap,
        jdw.nama_sub_tahap,
        jdw.waktu_mulai,
        jdw.waktu_selesai,
        jdw.id_sub_rkpd,
        sthp.sub_rkpd_teks,
        jdw.no_perda,
        jdw.tgl_perda,
        jdw.no_perkada,
        jdw.tgl_perkada,
        jdw.is_rinci_bl, 
        jdw.is_locked
    FROM
        d_jadwal jdw
         LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
            id_daerah,
            nama_daerah,
            id_prop
            FROM public.r_daerah' ) AS nd (id_daerah INT, nama_daerah VARCHAR, id_prop INT)ON nd.id_daerah = jdw.id_daerah
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
            id_daerah,
            nama_daerah,
            id_prop
            FROM public.r_daerah' ) AS np (id_daerah INT, nama_daerah VARCHAR, id_prop INT)ON np.id_daerah = nd.id_prop
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
            id_tahap,
            nama_tahap
            from public.r_tahapan') as thp (id_tahap INT, nama_tahap VARCHAR) ON thp.id_tahap=jdw.id_tahap
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
            id_sub_rkpd,
            sub_rkpd_teks
            from public.r_sub_rkpd') as sthp (id_sub_rkpd INT, sub_rkpd_teks VARCHAR) ON sthp.id_sub_rkpd = jdw.id_sub_rkpd
    where jdw.id_daerah = $1 and jdw.tahun = $2
    ORDER BY id_daerah,id_jadwal,id_tahap,id_sub_rkpd,is_rinci_bl,is_locked`;
        const value = [idDaerah, tahun];
        const result = await connection.select(listJadwal, value, daerah);

        const queryData = result.rows;
        // jika data tidak ditemukan di database
       
        const custom = queryData.map((item) => {
            if (item.is_locked === 1) {
                item.is_locked = 'Selesai dan Dikunci';
            }
            return {
                idDaerah: item.id_daerah,
                namaDaerah: item.nama_daerah,
                idProp: item.id_prop,
                namaProp: item.nama_prop,
                idJadwal: item.id_jadwal,
                tahun: item.tahun,
                idTahap: item.id_tahap,
                namaTahap: item.nama_tahap,
                namaSubTahap: item.nama_sub_tahap,
                waktuMulai: item.waktu_mulai,
                waktuSelesai: item.waktu_selesai,
                idSubRkpd: item.id_sub_rkpd,
                subRkpdTeks: item.sub_rkpd_teks,
                noPerda: item.no_perda,
                tglPerda: item.tgl_perda,
                noPerkada: item.no_perkada,
                tglPerkada: item.tgl_perkada,
                isRinciBl: item.is_rinci_bl,
                isLocked: item.is_locked
            }
        });
        const customResponse = {
            message: 'List Jadwal Daerah',
            code: 200,
            data: custom
        }
        return res.status(200).json(customResponse);


    } catch (error) {
        console.error(error);
        res.status(500).json({ error: error.message });
    }
};

module.exports = { listJadwalDaerah };
