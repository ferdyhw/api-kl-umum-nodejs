
const connection = require('../config');
require('dotenv').config();
// const NodeCache = require('node-cache');

const {
    DB_HOST_DBLINK_MASTER
} = process.env;

// const cache = new NodeCache();
const jadwalDaerah = async (req, res) => {
    try {
        const { tahun, idDaerah } = req.body;

        const daerah = req.body.kodeProp;
        connection.connect(daerah);

        const rincian = `WITH MaxJadwal AS (
            SELECT
                id_daerah,
                MAX(id_jadwal) AS max_id_jadwal
            FROM
                d_jadwal
            WHERE
                tahun = ${tahun}
                        AND is_locked = 1
            GROUP BY
                id_daerah
        )
        SELECT
            t1.id_jadwal,
            t1.nama_sub_tahap,
            t1.id_tahap,
            thp.nama_tahap,
            t1.id_daerah,
            t1.tahun,
            t1.waktu_selesai
        FROM
            d_jadwal t1
            LEFT JOIN dblink ('${DB_HOST_DBLINK_MASTER}', 'SELECT id_tahap, nama_tahap FROM r_tahapan') AS thp (id_tahap INT4, nama_tahap VARCHAR) ON t1.id_tahap = thp.id_tahap
      JOIN MaxJadwal mj ON t1.id_daerah = mj.id_daerah AND t1.id_jadwal = mj.max_id_jadwal
        WHERE
            t1.tahun = $1 and
            t1.id_daerah = $2
        ORDER BY
            t1.id_daerah ASC`;
        const value = [tahun, idDaerah];
        const result = await connection.select(rincian, value, daerah);
        const queryData = result.rows;

        // jika data tidak ditemukan di database
        if (queryData.length === 0) {
            return res.status(500).json({ error: 'Data tidak ditemukan' });
        }

        const custom = {
                idDaerah: queryData[0].id_daerah,
                idJadwal: queryData[0].id_jadwal,
                namaJadwal: queryData[0].nama_sub_tahap,
                idTahap: queryData[0].id_tahap,
                namaTahapan: queryData[0].nama_tahap,
                tahun: queryData[0].tahun,
                tanggalSelesai: queryData[0].waktu_selesai
            }
        const customResponse = {
            message: 'Jadwal Daerah',
            code: 200,
            data: custom
        }
        return res.status(200).json(customResponse);


    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};

module.exports = { jadwalDaerah };
