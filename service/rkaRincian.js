const connection = require("../config");
require("dotenv").config();

const { DB_HOST_DBLINK_MASTER, DB_HOST_DBLINK_SH } = process.env;

const rkaRincian = async (req, res) => {
  try {
    const { tahun, idDaerah, idJadwal, page, size } = req.body;
    const daerah = req.body.kodeProp;
    connection.connect(daerah);
    const komponen = `PUBLIC.d_komponen_${daerah}`;
    const pageNumber = parseInt(page) || 1;
    let pageSize = parseInt(size);

    let clientId = req.body.clientId;
    let tahapan = req.body.idTahapan;
    if (tahapan == 5) {
      tahapan = "rapbd";
    } else if (tahapan == 8) {
      tahapan = "rapbdperubahan";
    } else if (tahapan == 28) {
      tahapan = "penetapanapbd";
    } else if (tahapan == 29) {
      tahapan = "penetapanapbdperubahan";
    } else if (tahapan == 30) {
      tahapan = "penetapanpergeseran";
    } else if (tahapan == 32) {
      tahapan = "apbdpergeseransetelahperubahan";
    } else if (tahapan == 40) {
      tahapan = "kuappas";
    } else if (tahapan == 41) {
      tahapan = "kuapappas";
    } else {
      tahapan = "public";
    }

    let kondisi = "";
    let kondisi2 = "";

    // jika client id tidak ada maka kondisi = ''
    if (!clientId) {
      kondisi = "";
      kondisi2 = "";
    }
    // clientId khusus === 'WsN6c5(U6i0GnNw' || 'HYmXtKbOqZvw7d5'
    if (clientId === "p9!MQuNpFe%FZ4!" || clientId === "HYmXtKbOqZvw7d5") {
      kondisi = `AND set_input = 1 AND is_gaji_asn =0 AND is_btt = 0 AND is_bagi_hasil = 0`;
      kondisi2 = `AND (
                jenis.kode_akun = '5.1.02' 
                OR jenis.kode_akun = '5.1.06' 
                OR jenis.kode_akun = '5.1.05' 
                OR kelompok.kode_akun = '5.2' 
                OR kelompok.kode_akun = '5.3' 
            ) `;
    }

    let idSkpd = req.body.idSkpd;

    if (idSkpd == "") {
      idSkpd = null;
    }

    let idUnitSkpd = req.body.idUnitSkpd;

    if (idUnitSkpd == "") {
      idUnitSkpd = null;
    }

    //  jika pageSize = 0 ambil data dari variabel totalData
    if (pageSize === 0) {
      const totalDataQuery = `SELECT
            COUNT (*) as count 
            FROM
            ${tahapan}.d_sub_bl sbl
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT id_daerah,nama_daerah, kode_prop, kode_kab, kode_ddn from r_daerah WHERE
                is_deleted = 0
                AND kode_ddn NOT LIKE ''xx.yy''
                AND kode_ddn NOT LIKE ''00''
                AND kode_ddn NOT LIKE ''xx.xy'' ' ) AS nd ( id_daerah INT4, nama_daerah VARCHAR ( 500 ), kode_prop VARCHAR, kode_kab VARCHAR, kode_ddn VARCHAR ) ON sbl.id_daerah = nd.id_daerah /*get data daerah*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_skpd,
                    tahun,
                    id_daerah,
                    kode_unit,
                    kode_skpd,
                    nama_skpd,
                    id_unit,
                    is_skpd
                    FROM public.r_skpd
                    WHERE tahun = ${tahun}
                    AND id_daerah = ${idDaerah}' ) AS ss (
                    id_skpd INT4,
                    tahun INT4,
                    id_daerah INT4,
                    kode_unit VARCHAR ( 30 ),
                    kode_skpd VARCHAR ( 30 ),
                    nama_skpd VARCHAR ( 500 ),
                    id_unit INT4,
                    is_skpd INT4
                ) ON ss.tahun = sbl.tahun
                AND ss.id_daerah = sbl.id_daerah
                AND ss.id_skpd = sbl.id_skpd /*get data skpd*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_skpd,
                    tahun,
                    id_daerah,
                    kode_unit,
                    kode_skpd,
                    nama_skpd,
                    id_unit
                    FROM public.r_skpd
                    WHERE tahun = ${tahun}
                    AND id_daerah = ${idDaerah}' ) AS sss (
                    id_skpd INT4,
                    tahun INT4,
                    id_daerah INT4,
                    kode_unit VARCHAR ( 30 ),
                    kode_skpd VARCHAR ( 30 ),
                    nama_skpd VARCHAR ( 500 ),
                    id_unit INT4
                ) ON sss.tahun = sbl.tahun
                AND sss.id_daerah = sbl.id_daerah
                AND sss.id_skpd = sbl.id_sub_skpd /*get data sub skpd*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_urusan,
                    tahun,
                    id_daerah,
                    kode_urusan,
                    nama_urusan,
                    is_locked
                    FROM public.r_urusan
                WHERE tahun = ${tahun}' ) AS uu ( id_urusan INT4, tahun INT4, id_daerah INT4, kode_urusan VARCHAR ( 2 ), nama_urusan VARCHAR ( 500 ), is_locked INT4 ) ON uu.tahun = sbl.tahun
                AND uu.id_urusan = sbl.id_urusan /*get data urusan*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_bidang_urusan,
                    tahun,
                    id_urusan,
                    kode_bidang_urusan,
                    nama_bidang_urusan,
                    is_locked
                    FROM public.r_bidang_urusan
                WHERE tahun = ${tahun}' ) AS bu ( id_bidang_urusan INT4, tahun INT4, id_urusan INT4, kode_bidang_urusan VARCHAR ( 5 ), nama_bidang_urusan VARCHAR ( 500 ), is_locked INT4 ) ON bu.tahun = sbl.tahun
                AND bu.id_bidang_urusan = sbl.id_bidang_urusan /*get data bidang urusan*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_program,
                    tahun,
                    id_daerah,
                    id_urusan,
                    id_bidang_urusan,
                    kode_program,
                    nama_program
                    FROM public.r_program
                WHERE tahun = ${tahun}' ) AS pp ( id_program INT4, tahun INT4, id_daerah INT4, id_urusan INT4, id_bidang_urusan INT4, kode_program VARCHAR ( 10 ), nama_program VARCHAR ( 300 ) ) ON pp.tahun = sbl.tahun
                AND pp.id_program = sbl.id_program /*get data program*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_giat,
                    tahun,
                    id_daerah,
                    id_urusan,
                    id_bidang_urusan,
                    id_program,
                    kode_giat,
                    nama_giat
                    FROM public.r_giat
                    WHERE tahun = ${tahun}
                    ' ) AS gt (
                    id_giat INT4,
                    tahun INT4,
                    id_daerah INT4,
                    id_urusan INT4,
                    id_bidang_urusan INT4,
                    id_program INT4,
                    kode_giat VARCHAR ( 25 ),
                    nama_giat VARCHAR ( 500 )
                ) ON gt.tahun = sbl.tahun
                AND gt.id_program = sbl.id_program
                AND gt.id_giat = sbl.id_giat /*get data kegiatan*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_sub_giat,
                    tahun,
                    id_daerah,
                    id_urusan,
                    id_bidang_urusan,
                    id_program,
                    id_giat,
                    kode_sub_giat,
                    nama_sub_giat
                    FROM public.r_sub_giat
                    WHERE tahun = ${tahun}' ) AS sgt (
                    id_sub_giat INT8,
                    tahun INT4,
                    id_daerah INT4,
                    id_urusan INT4,
                    id_bidang_urusan INT4,
                    id_program INT4,
                    id_giat INT8,
                    kode_sub_giat VARCHAR ( 25 ),
                    nama_sub_giat VARCHAR ( 500 )
                ) ON sgt.tahun = sbl.tahun
                AND sgt.id_program = sbl.id_program
                AND sgt.id_giat = sbl.id_giat
                AND sgt.id_sub_giat = sbl.id_sub_giat
                LEFT JOIN ${tahapan}.d_rinci_sub_bl AS rinci ON rinci.id_daerah = sbl.id_daerah
                AND rinci.tahun = sbl.tahun
                AND rinci.id_sub_bl = sbl.id_sub_bl AND sbl.id_jadwal=rinci.id_jadwal /*get data sub kegiatan*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_akun,
                    tahun,
                    kode_akun,
                    nama_akun,
                    is_gaji_asn,
                    is_btt,
                    is_hibah_brg,
                    is_hibah_uang,
                    is_sosial_brg,
                    is_sosial_uang
                    FROM public.r_akun
                    WHERE tahun = ${tahun} AND is_bl = 1 ${kondisi}
                    ' ) AS sakun (
                    id_akun INT4,
                    tahun INT4,
                    kode_akun VARCHAR ( 50 ),
                    nama_akun VARCHAR ( 500 ),
                    is_gaji_asn INT4,
                    is_btt INT4,
                    is_hibah_brg INT4,
                    is_hibah_uang INT4,
                    is_sosial_brg INT4,
                    is_sosial_uang INT4
                ) ON sakun.tahun = rinci.tahun
                AND sakun.id_akun = rinci.id_akun /* get data sro*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_akun,
                    tahun,
                    kode_akun,
                    nama_akun,
                    is_gaji_asn,
                    is_btt,
                    is_hibah_brg,
                    is_hibah_uang,
                    is_sosial_brg,
                    is_sosial_uang
                    FROM public.r_akun
                    WHERE tahun = ${tahun}
                    ' ) AS akun (
                    id_akun INT4,
                    tahun INT4,
                    kode_akun VARCHAR ( 50 ),
                    nama_akun VARCHAR ( 500 ),
                    is_gaji_asn INT4,
                    is_btt INT4,
                    is_hibah_brg INT4,
                    is_hibah_uang INT4,
                    is_sosial_brg INT4,
                    is_sosial_uang INT4
                ) ON akun.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 1 ) /*get data akun*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_akun,
                    tahun,
                    kode_akun,
                    nama_akun,
                    is_gaji_asn,
                    is_btt,
                    is_hibah_brg,
                    is_hibah_uang,
                    is_sosial_brg,
                    is_sosial_uang
                    FROM public.r_akun
                    WHERE tahun = ${tahun} AND is_bl = 1
                    ' ) AS kelompok (
                    id_akun INT4,
                    tahun INT4,
                    kode_akun VARCHAR ( 50 ),
                    nama_akun VARCHAR ( 500 ),
                    is_gaji_asn INT4,
                    is_btt INT4,
                    is_hibah_brg INT4,
                    is_hibah_uang INT4,
                    is_sosial_brg INT4,
                    is_sosial_uang INT4
                ) ON kelompok.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 3 ) /*get data kelompok*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_akun,
                    tahun,
                    kode_akun,
                    nama_akun,
                    is_gaji_asn,
                    is_btt,
                    is_hibah_brg,
                    is_hibah_uang,
                    is_sosial_brg,
                    is_sosial_uang
                    FROM public.r_akun
                    WHERE tahun = ${tahun}
                    ' ) AS jenis (
                    id_akun INT4,
                    tahun INT4,
                    kode_akun VARCHAR ( 50 ),
                    nama_akun VARCHAR ( 500 ),
                    is_gaji_asn INT4,
                    is_btt INT4,
                    is_hibah_brg INT4,
                    is_hibah_uang INT4,
                    is_sosial_brg INT4,
                    is_sosial_uang INT4
                ) ON jenis.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 6 ) /*get data jenis*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_akun,
                    tahun,
                    kode_akun,
                    nama_akun,
                    is_gaji_asn,
                    is_btt,
                    is_hibah_brg,
                    is_hibah_uang,
                    is_sosial_brg,
                    is_sosial_uang
                    FROM public.r_akun
                    WHERE tahun = ${tahun}
                    ' ) AS objek (
                    id_akun INT4,
                    tahun INT4,
                    kode_akun VARCHAR ( 50 ),
                    nama_akun VARCHAR ( 500 ),
                    is_gaji_asn INT4,
                    is_btt INT4,
                    is_hibah_brg INT4,
                    is_hibah_uang INT4,
                    is_sosial_brg INT4,
                    is_sosial_uang INT4
                ) ON objek.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 9 ) /*get data objek*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_akun,
                    tahun,
                    kode_akun,
                    nama_akun,
                    is_gaji_asn,
                    is_btt,
                    is_hibah_brg,
                    is_hibah_uang,
                    is_sosial_brg,
                    is_sosial_uang
                    FROM public.r_akun
                    WHERE tahun = ${tahun}
                    ' ) AS rincian_objek (
                    id_akun INT4,
                    tahun INT4,
                    kode_akun VARCHAR ( 50 ),
                    nama_akun VARCHAR ( 500 ),
                    is_gaji_asn INT4,
                    is_btt INT4,
                    is_hibah_brg INT4,
                    is_hibah_uang INT4,
                    is_sosial_brg INT4,
                    is_sosial_uang INT4
                ) ON rincian_objek.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 12 ) /*get data rincian objek*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_dana,
                    tahun,
                    kode_dana,
                    nama_dana
                    FROM public.r_sumber_dana
                WHERE tahun = ${tahun}' ) AS sdana ( id_dana INT4, tahun INT4, kode_dana VARCHAR ( 50 ), nama_dana VARCHAR ( 500 ) ) ON sdana.tahun = sbl.tahun
                AND rinci.id_dana = sdana.id_dana /*get data sumber dana*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                    id_dana,
                    tahun,
                    kode_dana,
                    nama_dana
                    FROM public.r_sumber_dana
                WHERE tahun = ${tahun} and is_locked =0' ) AS akun_dana ( id_dana INT4, tahun INT4, kode_dana VARCHAR ( 50 ), nama_dana VARCHAR ( 500 ) ) ON sdana.tahun = sbl.tahun
                AND SUBSTRING ( sdana.kode_dana, 1, 1 ) = akun_dana.kode_dana /*get data sumber dana segemn 1*/
                LEFT JOIN ${tahapan}.d_subs_sub_bl pkt ON rinci.id_daerah = pkt.id_daerah
                AND rinci.tahun = pkt.tahun
                AND rinci.id_sub_bl = pkt.id_sub_bl
                AND rinci.id_subs_sub_bl = pkt.id_subs_sub_bl AND sbl.id_jadwal=pkt.id_jadwal /*get data subs_bl_teks*/
                LEFT JOIN ${tahapan}.d_ket_sub_bl ket ON rinci.id_daerah = ket.id_daerah
                AND rinci.tahun = ket.tahun
                AND rinci.id_sub_bl = ket.id_sub_bl
                AND rinci.id_ket_sub_bl = ket.id_ket_sub_bl AND sbl.id_jadwal=ket.id_jadwal /*get data ket_bl_teks*/
                LEFT JOIN dblink ( '${DB_HOST_DBLINK_SH}', 'SELECT  id_daerah,id_standar_harga, kode_standar_harga, nama_standar_harga, satuan, spek FROM ${komponen}
                WHERE tahun = ${tahun}' ) AS sh ( id_daerah INT, id_standar_harga INT, kode_standar_harga CHARACTER VARYING, nama_standar_harga CHARACTER VARYING, satuan CHARACTER VARYING, spek CHARACTER VARYING ) ON sh.id_standar_harga = rinci.id_standar_harga
                AND sh.id_daerah = rinci.id_daerah
            WHERE
                sbl.tahun = $1
                AND sbl.id_daerah = $2
                AND sbl.kunci_bl IN ( 0, 1 )
                 AND ( ( $3 :: INT IS NULL OR sbl.id_skpd = $3:: INT ) AND ( $4 :: INT IS NULL OR sbl.id_sub_skpd = $4 :: INT ) )
                AND sbl.id_jadwal = $5
                 ${kondisi2}`;
      const totalDataResult = await connection.select(
        totalDataQuery,
        [tahun, idDaerah, idSkpd, idUnitSkpd, idJadwal],
        daerah
      );
      const totalData = parseInt(totalDataResult.rows[0].count);
      pageSize = totalData;
    }
    // Hitung offset berdasarkan halaman dan ukuran halaman
    const offset = (pageNumber - 1) * pageSize;

    const rincian = `SELECT
    sbl.id_sub_bl,
    ( SELECT id_jadwal FROM d_jadwal WHERE tahun = ${tahun} AND id_daerah = ${idDaerah} AND is_locked = 1 ORDER BY waktu_selesai DESC LIMIT 1 ) AS id_jadwal,
    ( SELECT id_tahap FROM d_jadwal WHERE tahun = ${tahun} AND id_daerah = ${idDaerah} AND is_locked = 1 ORDER BY waktu_selesai DESC LIMIT 1 ) AS id_tahap,
    (
    SELECT
        thp.nama_tahap
    FROM
        d_jadwal jdw
        LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT id_tahap,nama_tahap FROM r_tahapan' ) AS thp ( id_tahap INT4, nama_tahap VARCHAR ) ON jdw.id_tahap = thp.id_tahap
    WHERE
        jdw.tahun = ${tahun}
        AND jdw.id_daerah = ${idDaerah}
        AND jdw.is_locked = 1
    ORDER BY
        jdw.waktu_selesai DESC
        LIMIT 1
    ) AS nama_tahap,
/*get data master tahapan*/
    ( SELECT nama_sub_tahap FROM d_jadwal WHERE tahun = ${tahun} AND id_daerah = ${idDaerah} AND is_locked = 1 ORDER BY waktu_selesai DESC LIMIT 1 ) AS nama_jadwal,
    sbl.tahun,
    nd.kode_prop,
    ( SELECT nama_daerah FROM r_daerah WHERE kode_prop = nd.kode_prop AND is_deleted = 0 AND is_prop = 1 LIMIT 1 ) AS nama_prop,
    sbl.id_daerah,
    nd.kode_kab,
    nd.nama_daerah,
    nd.kode_ddn,
    sbl.id_skpd,
    ss.kode_skpd,
    ss.nama_skpd,
    sbl.id_urusan,
    uu.kode_urusan,
    uu.nama_urusan,
    sbl.id_bidang_urusan,
    bu.kode_bidang_urusan,
    bu.nama_bidang_urusan,
    sbl.id_sub_skpd,
    sss.kode_skpd AS kode_sub_skpd,
    sss.nama_skpd AS nama_sub_skpd,
    sbl.id_program,
CASE

        WHEN "substring" ( pp.kode_program :: TEXT, 1, 4 ) = 'X.XX' :: TEXT THEN
        ( bu.kode_bidang_urusan :: TEXT || "substring" ( pp.kode_program :: TEXT, 5, 3 ) ) :: TEXT ELSE pp.kode_program
    END AS kode_program,
    pp.nama_program,
    sbl.id_giat,
CASE

        WHEN "substring" ( gt.kode_giat :: TEXT, 1, 4 ) = 'X.XX' :: TEXT THEN
        ( bu.kode_bidang_urusan :: TEXT || "substring" ( gt.kode_giat :: TEXT, 5, 20 ) ) :: TEXT ELSE gt.kode_giat
    END AS kode_giat,
    gt.nama_giat,
    sbl.id_sub_giat,
CASE

        WHEN "substring" ( sgt.kode_sub_giat :: TEXT, 1, 4 ) = 'X.XX' :: TEXT THEN
        ( bu.kode_bidang_urusan :: TEXT || "substring" ( sgt.kode_sub_giat :: TEXT, 5, 20 ) ) :: TEXT ELSE sgt.kode_sub_giat
    END AS kode_sub_giat,
/*menganti kode X.XX pada kode sub kegiatan*/
    sgt.nama_sub_giat,
    rinci.id_subs_sub_bl,
    CONCAT ( '[#] ', COALESCE ( pkt.subs_bl_teks, '' ) ) AS pkt_klpmk,
    rinci.id_ket_sub_bl,
    CONCAT ( '[-] ', COALESCE ( ket.ket_bl_teks, '' ) ) AS keterangan,
    akun_dana.id_dana AS id_akun_dana,
    akun_dana.kode_dana AS kode_akun_dana,
    akun_dana.nama_dana AS nama_akun_dana,
    sdana.kode_dana,
    sdana.nama_dana,
    sakun.id_akun,
    akun.kode_akun AS kode_akun,
    akun.nama_akun AS nama_akun,
    kelompok.id_akun AS id_kelompok,
    kelompok.kode_akun AS kode_kelompok,
    kelompok.nama_akun AS nama_kelompok,
    jenis.id_akun AS id_jenis,
    jenis.kode_akun AS kode_jenis,
    jenis.nama_akun AS nama_jenis,
    objek.id_akun AS id_objek,
    objek.kode_akun AS kode_objek,
    objek.nama_akun AS nama_objek,
    rincian_objek.id_akun AS id_rincian_objek,
    rincian_objek.kode_akun AS kode_rincian_objek,
    rincian_objek.nama_akun AS nama_rincian_objek,
    sakun.kode_akun AS kode_sub_rincian_objek,
    sakun.nama_akun AS nama_sub_rincian_objek,
    rinci.id_rinci_sub_bl,
    rinci.id_standar_harga,
    sh.kode_standar_harga,
    sh.nama_standar_harga,
    sh.satuan,
    sh.spek,
    rinci.pajak,
    rinci.vol_1,
    rinci.sat_1,
    rinci.vol_2,
    rinci.sat_2,
    rinci.vol_3,
    rinci.sat_3,
    rinci.vol_4,
    rinci.sat_4 volume,
    koefisien,
    rinci.total_harga AS rincian
FROM
${tahapan}.d_sub_bl sbl
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT id_daerah,nama_daerah, kode_prop, kode_kab, kode_ddn from r_daerah WHERE
    is_deleted = 0
    AND kode_ddn NOT LIKE ''xx.yy''
    AND kode_ddn NOT LIKE ''00''
    AND kode_ddn NOT LIKE ''xx.xy'' ' ) AS nd ( id_daerah INT4, nama_daerah VARCHAR ( 500 ), kode_prop VARCHAR, kode_kab VARCHAR, kode_ddn VARCHAR ) ON sbl.id_daerah = nd.id_daerah /*get data daerah*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_skpd,
        tahun,
        id_daerah,
        kode_unit,
        kode_skpd,
        nama_skpd,
        id_unit,
        is_skpd
        FROM public.r_skpd
        WHERE tahun = ${tahun}
        AND id_daerah = ${idDaerah}' ) AS ss (
        id_skpd INT4,
        tahun INT4,
        id_daerah INT4,
        kode_unit VARCHAR ( 30 ),
        kode_skpd VARCHAR ( 30 ),
        nama_skpd VARCHAR ( 500 ),
        id_unit INT4,
        is_skpd INT4
    ) ON ss.tahun = sbl.tahun
    AND ss.id_daerah = sbl.id_daerah
    AND ss.id_skpd = sbl.id_skpd /*get data skpd*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_skpd,
        tahun,
        id_daerah,
        kode_unit,
        kode_skpd,
        nama_skpd,
        id_unit
        FROM public.r_skpd
        WHERE tahun = ${tahun}
        AND id_daerah = ${idDaerah}' ) AS sss (
        id_skpd INT4,
        tahun INT4,
        id_daerah INT4,
        kode_unit VARCHAR ( 30 ),
        kode_skpd VARCHAR ( 30 ),
        nama_skpd VARCHAR ( 500 ),
        id_unit INT4
    ) ON sss.tahun = sbl.tahun
    AND sss.id_daerah = sbl.id_daerah
    AND sss.id_skpd = sbl.id_sub_skpd /*get data sub skpd*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_urusan,
        tahun,
        id_daerah,
        kode_urusan,
        nama_urusan,
        is_locked
        FROM public.r_urusan
    WHERE tahun = ${tahun}' ) AS uu ( id_urusan INT4, tahun INT4, id_daerah INT4, kode_urusan VARCHAR ( 2 ), nama_urusan VARCHAR ( 500 ), is_locked INT4 ) ON uu.tahun = sbl.tahun
    AND uu.id_urusan = sbl.id_urusan /*get data urusan*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_bidang_urusan,
        tahun,
        id_urusan,
        kode_bidang_urusan,
        nama_bidang_urusan,
        is_locked
        FROM public.r_bidang_urusan
    WHERE tahun = ${tahun}' ) AS bu ( id_bidang_urusan INT4, tahun INT4, id_urusan INT4, kode_bidang_urusan VARCHAR ( 5 ), nama_bidang_urusan VARCHAR ( 500 ), is_locked INT4 ) ON bu.tahun = sbl.tahun
    AND bu.id_bidang_urusan = sbl.id_bidang_urusan /*get data bidang urusan*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_program,
        tahun,
        id_daerah,
        id_urusan,
        id_bidang_urusan,
        kode_program,
        nama_program
        FROM public.r_program
    WHERE tahun = ${tahun}' ) AS pp ( id_program INT4, tahun INT4, id_daerah INT4, id_urusan INT4, id_bidang_urusan INT4, kode_program VARCHAR ( 10 ), nama_program VARCHAR ( 300 ) ) ON pp.tahun = sbl.tahun
    AND pp.id_program = sbl.id_program /*get data program*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_giat,
        tahun,
        id_daerah,
        id_urusan,
        id_bidang_urusan,
        id_program,
        kode_giat,
        nama_giat
        FROM public.r_giat
        WHERE tahun = ${tahun}
        ' ) AS gt (
        id_giat INT4,
        tahun INT4,
        id_daerah INT4,
        id_urusan INT4,
        id_bidang_urusan INT4,
        id_program INT4,
        kode_giat VARCHAR ( 25 ),
        nama_giat VARCHAR ( 500 )
    ) ON gt.tahun = sbl.tahun
    AND gt.id_program = sbl.id_program
    AND gt.id_giat = sbl.id_giat /*get data kegiatan*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_sub_giat,
        tahun,
        id_daerah,
        id_urusan,
        id_bidang_urusan,
        id_program,
        id_giat,
        kode_sub_giat,
        nama_sub_giat
        FROM public.r_sub_giat
        WHERE tahun = ${tahun}' ) AS sgt (
        id_sub_giat INT8,
        tahun INT4,
        id_daerah INT4,
        id_urusan INT4,
        id_bidang_urusan INT4,
        id_program INT4,
        id_giat INT8,
        kode_sub_giat VARCHAR ( 25 ),
        nama_sub_giat VARCHAR ( 500 )
    ) ON sgt.tahun = sbl.tahun
    AND sgt.id_program = sbl.id_program
    AND sgt.id_giat = sbl.id_giat
    AND sgt.id_sub_giat = sbl.id_sub_giat
    LEFT JOIN ${tahapan}.d_rinci_sub_bl AS rinci ON rinci.id_daerah = sbl.id_daerah
    AND rinci.tahun = sbl.tahun
    AND rinci.id_sub_bl = sbl.id_sub_bl AND sbl.id_jadwal=rinci.id_jadwal /*get data sub kegiatan*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_akun,
        tahun,
        kode_akun,
        nama_akun,
        is_gaji_asn,
        is_btt,
        is_hibah_brg,
        is_hibah_uang,
        is_sosial_brg,
        is_sosial_uang
        FROM public.r_akun
        WHERE tahun = ${tahun} AND is_bl = 1 ${kondisi}
        ' ) AS sakun (
        id_akun INT4,
        tahun INT4,
        kode_akun VARCHAR ( 50 ),
        nama_akun VARCHAR ( 500 ),
        is_gaji_asn INT4,
        is_btt INT4,
        is_hibah_brg INT4,
        is_hibah_uang INT4,
        is_sosial_brg INT4,
        is_sosial_uang INT4
    ) ON sakun.tahun = rinci.tahun
    AND sakun.id_akun = rinci.id_akun /* get data sro*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_akun,
        tahun,
        kode_akun,
        nama_akun,
        is_gaji_asn,
        is_btt,
        is_hibah_brg,
        is_hibah_uang,
        is_sosial_brg,
        is_sosial_uang
        FROM public.r_akun
        WHERE tahun = ${tahun}
        ' ) AS akun (
        id_akun INT4,
        tahun INT4,
        kode_akun VARCHAR ( 50 ),
        nama_akun VARCHAR ( 500 ),
        is_gaji_asn INT4,
        is_btt INT4,
        is_hibah_brg INT4,
        is_hibah_uang INT4,
        is_sosial_brg INT4,
        is_sosial_uang INT4
    ) ON akun.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 1 ) /*get data akun*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_akun,
        tahun,
        kode_akun,
        nama_akun,
        is_gaji_asn,
        is_btt,
        is_hibah_brg,
        is_hibah_uang,
        is_sosial_brg,
        is_sosial_uang
        FROM public.r_akun
        WHERE tahun = ${tahun} AND is_bl = 1
        ' ) AS kelompok (
        id_akun INT4,
        tahun INT4,
        kode_akun VARCHAR ( 50 ),
        nama_akun VARCHAR ( 500 ),
        is_gaji_asn INT4,
        is_btt INT4,
        is_hibah_brg INT4,
        is_hibah_uang INT4,
        is_sosial_brg INT4,
        is_sosial_uang INT4
    ) ON kelompok.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 3 ) /*get data kelompok*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_akun,
        tahun,
        kode_akun,
        nama_akun,
        is_gaji_asn,
        is_btt,
        is_hibah_brg,
        is_hibah_uang,
        is_sosial_brg,
        is_sosial_uang
        FROM public.r_akun
        WHERE tahun = ${tahun}
        ' ) AS jenis (
        id_akun INT4,
        tahun INT4,
        kode_akun VARCHAR ( 50 ),
        nama_akun VARCHAR ( 500 ),
        is_gaji_asn INT4,
        is_btt INT4,
        is_hibah_brg INT4,
        is_hibah_uang INT4,
        is_sosial_brg INT4,
        is_sosial_uang INT4
    ) ON jenis.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 6 ) /*get data jenis*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_akun,
        tahun,
        kode_akun,
        nama_akun,
        is_gaji_asn,
        is_btt,
        is_hibah_brg,
        is_hibah_uang,
        is_sosial_brg,
        is_sosial_uang
        FROM public.r_akun
        WHERE tahun = ${tahun}
        ' ) AS objek (
        id_akun INT4,
        tahun INT4,
        kode_akun VARCHAR ( 50 ),
        nama_akun VARCHAR ( 500 ),
        is_gaji_asn INT4,
        is_btt INT4,
        is_hibah_brg INT4,
        is_hibah_uang INT4,
        is_sosial_brg INT4,
        is_sosial_uang INT4
    ) ON objek.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 9 ) /*get data objek*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_akun,
        tahun,
        kode_akun,
        nama_akun,
        is_gaji_asn,
        is_btt,
        is_hibah_brg,
        is_hibah_uang,
        is_sosial_brg,
        is_sosial_uang
        FROM public.r_akun
        WHERE tahun = ${tahun}
        ' ) AS rincian_objek (
        id_akun INT4,
        tahun INT4,
        kode_akun VARCHAR ( 50 ),
        nama_akun VARCHAR ( 500 ),
        is_gaji_asn INT4,
        is_btt INT4,
        is_hibah_brg INT4,
        is_hibah_uang INT4,
        is_sosial_brg INT4,
        is_sosial_uang INT4
    ) ON rincian_objek.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 12 ) /*get data rincian objek*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_dana,
        tahun,
        kode_dana,
        nama_dana
        FROM public.r_sumber_dana
    WHERE tahun = ${tahun}' ) AS sdana ( id_dana INT4, tahun INT4, kode_dana VARCHAR ( 50 ), nama_dana VARCHAR ( 500 ) ) ON sdana.tahun = sbl.tahun
    AND rinci.id_dana = sdana.id_dana /*get data sumber dana*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
        id_dana,
        tahun,
        kode_dana,
        nama_dana
        FROM public.r_sumber_dana
    WHERE tahun = ${tahun} and is_locked =0' ) AS akun_dana ( id_dana INT4, tahun INT4, kode_dana VARCHAR ( 50 ), nama_dana VARCHAR ( 500 ) ) ON sdana.tahun = sbl.tahun
    AND SUBSTRING ( sdana.kode_dana, 1, 1 ) = akun_dana.kode_dana /*get data sumber dana segemn 1*/
    LEFT JOIN ${tahapan}.d_subs_sub_bl pkt ON rinci.id_daerah = pkt.id_daerah
    AND rinci.tahun = pkt.tahun
    AND rinci.id_sub_bl = pkt.id_sub_bl
    AND rinci.id_subs_sub_bl = pkt.id_subs_sub_bl AND sbl.id_jadwal=pkt.id_jadwal /*get data subs_bl_teks*/
    LEFT JOIN ${tahapan}.d_ket_sub_bl ket ON rinci.id_daerah = ket.id_daerah
    AND rinci.tahun = ket.tahun
    AND rinci.id_sub_bl = ket.id_sub_bl
    AND rinci.id_ket_sub_bl = ket.id_ket_sub_bl AND sbl.id_jadwal=ket.id_jadwal /*get data ket_bl_teks*/
    LEFT JOIN dblink ( '${DB_HOST_DBLINK_SH}', 'SELECT  id_daerah,id_standar_harga, kode_standar_harga, nama_standar_harga, satuan, spek FROM ${komponen}
    WHERE tahun = ${tahun}' ) AS sh ( id_daerah INT, id_standar_harga INT, kode_standar_harga CHARACTER VARYING, nama_standar_harga CHARACTER VARYING, satuan CHARACTER VARYING, spek CHARACTER VARYING ) ON sh.id_standar_harga = rinci.id_standar_harga
    AND sh.id_daerah = rinci.id_daerah
WHERE
    sbl.tahun = $1
    AND sbl.id_daerah = $2
    AND sbl.kunci_bl IN ( 0, 1 )
     AND ( ( $3 :: INT IS NULL OR sbl.id_skpd = $3:: INT ) AND ( $4 :: INT IS NULL OR sbl.id_sub_skpd = $4 :: INT ) )
    AND sbl.id_jadwal = $5
     ${kondisi2}
ORDER BY
     rinci.kode_standar_harga
    LIMIT $6 OFFSET $7`;
    const value = [
      tahun,
      idDaerah,
      idSkpd,
      idUnitSkpd,
      idJadwal,
      pageSize,
      offset,
    ];
    const result = await connection.select(rincian, value, daerah);
    const queryData = result.rows;

    const totalDataQuery = `SELECT
        COUNT (*) as count 
        FROM
        ${tahapan}.d_sub_bl sbl
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT id_daerah,nama_daerah, kode_prop, kode_kab, kode_ddn from r_daerah WHERE
            is_deleted = 0
            AND kode_ddn NOT LIKE ''xx.yy''
            AND kode_ddn NOT LIKE ''00''
            AND kode_ddn NOT LIKE ''xx.xy'' ' ) AS nd ( id_daerah INT4, nama_daerah VARCHAR ( 500 ), kode_prop VARCHAR, kode_kab VARCHAR, kode_ddn VARCHAR ) ON sbl.id_daerah = nd.id_daerah /*get data daerah*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_skpd,
                tahun,
                id_daerah,
                kode_unit,
                kode_skpd,
                nama_skpd,
                id_unit,
                is_skpd
                FROM public.r_skpd
                WHERE tahun = ${tahun}
                AND id_daerah = ${idDaerah}' ) AS ss (
                id_skpd INT4,
                tahun INT4,
                id_daerah INT4,
                kode_unit VARCHAR ( 30 ),
                kode_skpd VARCHAR ( 30 ),
                nama_skpd VARCHAR ( 500 ),
                id_unit INT4,
                is_skpd INT4
            ) ON ss.tahun = sbl.tahun
            AND ss.id_daerah = sbl.id_daerah
            AND ss.id_skpd = sbl.id_skpd /*get data skpd*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_skpd,
                tahun,
                id_daerah,
                kode_unit,
                kode_skpd,
                nama_skpd,
                id_unit
                FROM public.r_skpd
                WHERE tahun = ${tahun}
                AND id_daerah = ${idDaerah}' ) AS sss (
                id_skpd INT4,
                tahun INT4,
                id_daerah INT4,
                kode_unit VARCHAR ( 30 ),
                kode_skpd VARCHAR ( 30 ),
                nama_skpd VARCHAR ( 500 ),
                id_unit INT4
            ) ON sss.tahun = sbl.tahun
            AND sss.id_daerah = sbl.id_daerah
            AND sss.id_skpd = sbl.id_sub_skpd /*get data sub skpd*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_urusan,
                tahun,
                id_daerah,
                kode_urusan,
                nama_urusan,
                is_locked
                FROM public.r_urusan
            WHERE tahun = ${tahun}' ) AS uu ( id_urusan INT4, tahun INT4, id_daerah INT4, kode_urusan VARCHAR ( 2 ), nama_urusan VARCHAR ( 500 ), is_locked INT4 ) ON uu.tahun = sbl.tahun
            AND uu.id_urusan = sbl.id_urusan /*get data urusan*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_bidang_urusan,
                tahun,
                id_urusan,
                kode_bidang_urusan,
                nama_bidang_urusan,
                is_locked
                FROM public.r_bidang_urusan
            WHERE tahun = ${tahun}' ) AS bu ( id_bidang_urusan INT4, tahun INT4, id_urusan INT4, kode_bidang_urusan VARCHAR ( 5 ), nama_bidang_urusan VARCHAR ( 500 ), is_locked INT4 ) ON bu.tahun = sbl.tahun
            AND bu.id_bidang_urusan = sbl.id_bidang_urusan /*get data bidang urusan*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_program,
                tahun,
                id_daerah,
                id_urusan,
                id_bidang_urusan,
                kode_program,
                nama_program
                FROM public.r_program
            WHERE tahun = ${tahun}' ) AS pp ( id_program INT4, tahun INT4, id_daerah INT4, id_urusan INT4, id_bidang_urusan INT4, kode_program VARCHAR ( 10 ), nama_program VARCHAR ( 300 ) ) ON pp.tahun = sbl.tahun
            AND pp.id_program = sbl.id_program /*get data program*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_giat,
                tahun,
                id_daerah,
                id_urusan,
                id_bidang_urusan,
                id_program,
                kode_giat,
                nama_giat
                FROM public.r_giat
                WHERE tahun = ${tahun}
                ' ) AS gt (
                id_giat INT4,
                tahun INT4,
                id_daerah INT4,
                id_urusan INT4,
                id_bidang_urusan INT4,
                id_program INT4,
                kode_giat VARCHAR ( 25 ),
                nama_giat VARCHAR ( 500 )
            ) ON gt.tahun = sbl.tahun
            AND gt.id_program = sbl.id_program
            AND gt.id_giat = sbl.id_giat /*get data kegiatan*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_sub_giat,
                tahun,
                id_daerah,
                id_urusan,
                id_bidang_urusan,
                id_program,
                id_giat,
                kode_sub_giat,
                nama_sub_giat
                FROM public.r_sub_giat
                WHERE tahun = ${tahun}' ) AS sgt (
                id_sub_giat INT8,
                tahun INT4,
                id_daerah INT4,
                id_urusan INT4,
                id_bidang_urusan INT4,
                id_program INT4,
                id_giat INT8,
                kode_sub_giat VARCHAR ( 25 ),
                nama_sub_giat VARCHAR ( 500 )
            ) ON sgt.tahun = sbl.tahun
            AND sgt.id_program = sbl.id_program
            AND sgt.id_giat = sbl.id_giat
            AND sgt.id_sub_giat = sbl.id_sub_giat
            LEFT JOIN ${tahapan}.d_rinci_sub_bl AS rinci ON rinci.id_daerah = sbl.id_daerah
            AND rinci.tahun = sbl.tahun
            AND rinci.id_sub_bl = sbl.id_sub_bl AND sbl.id_jadwal=rinci.id_jadwal /*get data sub kegiatan*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_akun,
                tahun,
                kode_akun,
                nama_akun,
                is_gaji_asn,
                is_btt,
                is_hibah_brg,
                is_hibah_uang,
                is_sosial_brg,
                is_sosial_uang
                FROM public.r_akun
                WHERE tahun = ${tahun} AND is_bl = 1 ${kondisi}
                ' ) AS sakun (
                id_akun INT4,
                tahun INT4,
                kode_akun VARCHAR ( 50 ),
                nama_akun VARCHAR ( 500 ),
                is_gaji_asn INT4,
                is_btt INT4,
                is_hibah_brg INT4,
                is_hibah_uang INT4,
                is_sosial_brg INT4,
                is_sosial_uang INT4
            ) ON sakun.tahun = rinci.tahun
            AND sakun.id_akun = rinci.id_akun /* get data sro*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_akun,
                tahun,
                kode_akun,
                nama_akun,
                is_gaji_asn,
                is_btt,
                is_hibah_brg,
                is_hibah_uang,
                is_sosial_brg,
                is_sosial_uang
                FROM public.r_akun
                WHERE tahun = ${tahun}
                ' ) AS akun (
                id_akun INT4,
                tahun INT4,
                kode_akun VARCHAR ( 50 ),
                nama_akun VARCHAR ( 500 ),
                is_gaji_asn INT4,
                is_btt INT4,
                is_hibah_brg INT4,
                is_hibah_uang INT4,
                is_sosial_brg INT4,
                is_sosial_uang INT4
            ) ON akun.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 1 ) /*get data akun*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_akun,
                tahun,
                kode_akun,
                nama_akun,
                is_gaji_asn,
                is_btt,
                is_hibah_brg,
                is_hibah_uang,
                is_sosial_brg,
                is_sosial_uang
                FROM public.r_akun
                WHERE tahun = ${tahun} AND is_bl = 1
                ' ) AS kelompok (
                id_akun INT4,
                tahun INT4,
                kode_akun VARCHAR ( 50 ),
                nama_akun VARCHAR ( 500 ),
                is_gaji_asn INT4,
                is_btt INT4,
                is_hibah_brg INT4,
                is_hibah_uang INT4,
                is_sosial_brg INT4,
                is_sosial_uang INT4
            ) ON kelompok.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 3 ) /*get data kelompok*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_akun,
                tahun,
                kode_akun,
                nama_akun,
                is_gaji_asn,
                is_btt,
                is_hibah_brg,
                is_hibah_uang,
                is_sosial_brg,
                is_sosial_uang
                FROM public.r_akun
                WHERE tahun = ${tahun}
                ' ) AS jenis (
                id_akun INT4,
                tahun INT4,
                kode_akun VARCHAR ( 50 ),
                nama_akun VARCHAR ( 500 ),
                is_gaji_asn INT4,
                is_btt INT4,
                is_hibah_brg INT4,
                is_hibah_uang INT4,
                is_sosial_brg INT4,
                is_sosial_uang INT4
            ) ON jenis.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 6 ) /*get data jenis*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_akun,
                tahun,
                kode_akun,
                nama_akun,
                is_gaji_asn,
                is_btt,
                is_hibah_brg,
                is_hibah_uang,
                is_sosial_brg,
                is_sosial_uang
                FROM public.r_akun
                WHERE tahun = ${tahun}
                ' ) AS objek (
                id_akun INT4,
                tahun INT4,
                kode_akun VARCHAR ( 50 ),
                nama_akun VARCHAR ( 500 ),
                is_gaji_asn INT4,
                is_btt INT4,
                is_hibah_brg INT4,
                is_hibah_uang INT4,
                is_sosial_brg INT4,
                is_sosial_uang INT4
            ) ON objek.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 9 ) /*get data objek*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_akun,
                tahun,
                kode_akun,
                nama_akun,
                is_gaji_asn,
                is_btt,
                is_hibah_brg,
                is_hibah_uang,
                is_sosial_brg,
                is_sosial_uang
                FROM public.r_akun
                WHERE tahun = ${tahun}
                ' ) AS rincian_objek (
                id_akun INT4,
                tahun INT4,
                kode_akun VARCHAR ( 50 ),
                nama_akun VARCHAR ( 500 ),
                is_gaji_asn INT4,
                is_btt INT4,
                is_hibah_brg INT4,
                is_hibah_uang INT4,
                is_sosial_brg INT4,
                is_sosial_uang INT4
            ) ON rincian_objek.kode_akun = SUBSTRING ( sakun.kode_akun, 1, 12 ) /*get data rincian objek*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_dana,
                tahun,
                kode_dana,
                nama_dana
                FROM public.r_sumber_dana
            WHERE tahun = ${tahun}' ) AS sdana ( id_dana INT4, tahun INT4, kode_dana VARCHAR ( 50 ), nama_dana VARCHAR ( 500 ) ) ON sdana.tahun = sbl.tahun
            AND rinci.id_dana = sdana.id_dana /*get data sumber dana*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_MASTER}', 'SELECT
                id_dana,
                tahun,
                kode_dana,
                nama_dana
                FROM public.r_sumber_dana
            WHERE tahun = ${tahun} and is_locked =0' ) AS akun_dana ( id_dana INT4, tahun INT4, kode_dana VARCHAR ( 50 ), nama_dana VARCHAR ( 500 ) ) ON sdana.tahun = sbl.tahun
            AND SUBSTRING ( sdana.kode_dana, 1, 1 ) = akun_dana.kode_dana /*get data sumber dana segemn 1*/
            LEFT JOIN ${tahapan}.d_subs_sub_bl pkt ON rinci.id_daerah = pkt.id_daerah
            AND rinci.tahun = pkt.tahun
            AND rinci.id_sub_bl = pkt.id_sub_bl
            AND rinci.id_subs_sub_bl = pkt.id_subs_sub_bl AND sbl.id_jadwal=pkt.id_jadwal /*get data subs_bl_teks*/
            LEFT JOIN ${tahapan}.d_ket_sub_bl ket ON rinci.id_daerah = ket.id_daerah
            AND rinci.tahun = ket.tahun
            AND rinci.id_sub_bl = ket.id_sub_bl
            AND rinci.id_ket_sub_bl = ket.id_ket_sub_bl AND sbl.id_jadwal=ket.id_jadwal /*get data ket_bl_teks*/
            LEFT JOIN dblink ( '${DB_HOST_DBLINK_SH}', 'SELECT  id_daerah,id_standar_harga, kode_standar_harga, nama_standar_harga, satuan, spek FROM ${komponen}
            WHERE tahun = ${tahun}' ) AS sh ( id_daerah INT, id_standar_harga INT, kode_standar_harga CHARACTER VARYING, nama_standar_harga CHARACTER VARYING, satuan CHARACTER VARYING, spek CHARACTER VARYING ) ON sh.id_standar_harga = rinci.id_standar_harga
            AND sh.id_daerah = rinci.id_daerah
        WHERE
            sbl.tahun = $1
            AND sbl.id_daerah = $2
            AND sbl.kunci_bl IN ( 0, 1 )
             AND ( ( $3 :: INT IS NULL OR sbl.id_skpd = $3:: INT ) AND ( $4 :: INT IS NULL OR sbl.id_sub_skpd = $4 :: INT ) )
            AND sbl.id_jadwal = $5
             ${kondisi2}`;
    const totalDataResult = await connection.select(
      totalDataQuery,
      [tahun, idDaerah, idSkpd, idUnitSkpd, idJadwal],
      daerah
    );
    const totalData = parseInt(totalDataResult.rows[0].count);
    let totalPages = Math.ceil(totalData / pageSize);

    // jika totalPages = null maka totalPages = 1
    if (!totalPages) {
      totalPages = 0;
    }
    const custom = queryData.map((item) => {
      return {
        idRinciSubBl: parseInt(item.id_rinci_sub_bl),
        idSubBl: parseInt(item.id_sub_bl),
        idJadwal: item.id_jadwal,
        namaJadwal: item.nama_jadwal,
        idTahap: item.id_tahap,
        namaTahap: item.nama_tahap,
        tahun: item.tahun,
        kodeProp: item.kode_prop,
        namaProp: item.nama_prop,
        idDaerah: item.id_daerah,
        kodeKab: item.kode_kab,
        kodeDdn: item.kode_ddn,
        namaDaerah: item.nama_daerah,
        idSkpd: item.id_skpd,
        kodeSkpd: item.kode_skpd,
        namaSkpd: item.nama_skpd,
        idUrusan: item.id_urusan,
        kodeUrusan: item.kode_urusan,
        namaUrusan: item.nama_urusan,
        idBidangUrusan: item.id_bidang_urusan,
        kodeBidangUrusan: item.kode_bidang_urusan,
        namaBidangUrusan: item.nama_bidang_urusan,
        idSubSkpd: item.id_sub_skpd,
        kodeSubSkpd: item.kode_sub_skpd,
        namaSubSkpd: item.nama_sub_skpd,
        idProgram: item.id_program,
        kodeProgram: item.kode_program,
        namaProgram: item.nama_program,
        idGiat: parseInt(item.id_giat),
        kodeGiat: item.kode_giat,
        namaGiat: item.nama_giat,
        idSubGiat: parseInt(item.id_sub_giat),
        kodeSubGiat: item.kode_sub_giat,
        namaSubGiat: item.nama_sub_giat,
        idSubsSubBl: parseInt(item.id_subs_sub_bl),
        paket: item.pkt_klpmk,
        idKetSubBl: parseInt(item.id_ket_sub_bl),
        keterangan: item.keterangan,
        idAkunDana: item.id_akun_dana,
        kodeAkunDana: item.kode_akun_dana,
        namaAkunDana: item.nama_akun_dana,
        kodeDana: item.kode_dana,
        namaDana: item.nama_dana,
        idAkun: item.id_akun,
        kodeAkun: item.kode_akun,
        namaAkun: item.nama_akun,
        idKelompok: item.id_kelompok,
        kodeKelompok: item.kode_kelompok,
        namaKelompok: item.nama_kelompok,
        idJenis: item.id_jenis,
        kodeJenis: item.kode_jenis,
        namaJenis: item.nama_jenis,
        idObjek: item.id_objek,
        kodeObjek: item.kode_objek,
        namaObjek: item.nama_objek,
        idRincianObjek: item.id_rincian_objek,
        kodeRincianObjek: item.kode_rincian_objek,
        namaRincianObjek: item.nama_rincian_objek,
        kodeSubRincianObjek: item.kode_sub_rincian_objek,
        namaSubRincianObjek: item.nama_sub_rincian_objek,
        idStandarHarga: parseInt(item.id_standar_harga),
        kodeStandarHarga: item.kode_standar_harga,
        namaStandarHarga: item.nama_standar_harga,
        satuan: item.satuan,
        spek: item.spek,
        pajak: item.pajak,
        vol1: item.vol_1,
        sat1: item.sat_1,
        vol2: item.vol_2,
        sat2: item.sat_2,
        vol3: item.vol_3,
        sat3: item.sat_3,
        vol4: item.vol_4,
        sat4: item.sat_4,
        volume: item.volume,
        koefisien: item.koefisien,
        rincian: item.rincian,
      };
    });
    const customResponse = {
      message: "List Rincian RKA",
      tanggalPenarikan: new Date(),
      code: 200,
      data: custom,
      currentPage: pageNumber,
      totalPages: totalPages,
      totalData: totalData,
      pageSize: pageSize,
    };

    return res.status(200).json(customResponse);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

module.exports = { rkaRincian };
