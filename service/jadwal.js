
const connection = require('../config');
require('dotenv').config();
// const NodeCache = require('node-cache');

const {
    DB_HOST_DBLINK_MASTER
} = process.env;

// const cache = new NodeCache();
const jadwal = async (req, res) => {
    try {
        const { tahun } = req.body;

        const daerah = req.body.kodeProp;
        connection.connect(daerah);

        const { page, size } = req.body;
        const pageNumber = parseInt(page) || 1;
        const pageSize = parseInt(size) || 10;

        // Hitung offset berdasarkan halaman dan ukuran halaman
        const offset = (pageNumber - 1) * pageSize;

        // Definisikan key untuk caching berdasarkan parameter permintaan
        // const cacheKey = JSON.stringify(req.body);
        // // Hapus cache lama jika cacheKey telah berubah
        // if (cacheKey !== cache.get('lastCacheKey')) {
        //     cache.flushAll();
        //     cache.set('lastCacheKey', cacheKey);
        // }

        // Cek apakah data sudah ada di cache
        // const cachedData = cache.get(cacheKey);
        // if (cachedData) {
        //     console.log('Data ditemukan di cache');
        //     return res.status(200).json(cachedData);
        // }

        const rincian = `WITH MaxJadwal AS (
            SELECT
                id_daerah,
                MAX(id_jadwal) AS max_id_jadwal
            FROM
                d_jadwal
            WHERE
                tahun = ${tahun}
                        AND is_locked = 1
            GROUP BY
                id_daerah
        )
        SELECT
            t1.id_jadwal,
            t1.nama_sub_tahap,
            t1.id_tahap,
            thp.nama_tahap,
            t1.id_daerah,
            dd.nama_daerah,
            t1.tahun,
            t1.waktu_selesai,
            t1.is_locked
        FROM
            d_jadwal t1
        LEFT JOIN dblink ('${DB_HOST_DBLINK_MASTER}', 'SELECT id_daerah, nama_daerah FROM r_daerah WHERE is_deleted = 0') AS dd (id_daerah INT4, nama_daerah VARCHAR) ON t1.id_daerah = dd.id_daerah
        LEFT JOIN dblink ('${DB_HOST_DBLINK_MASTER}', 'SELECT id_tahap, nama_tahap FROM r_tahapan') AS thp (id_tahap INT4, nama_tahap VARCHAR) ON t1.id_tahap = thp.id_tahap
        JOIN MaxJadwal mj ON t1.id_daerah = mj.id_daerah AND t1.id_jadwal = mj.max_id_jadwal
        WHERE
            t1.tahun = $1
        ORDER BY
            t1.id_daerah ASC                            
        LIMIT ${pageSize} OFFSET ${offset}`;
        const value = [tahun];
        const result = await connection.select(rincian, value, daerah);


        const totalDataQuery = `WITH MaxJadwal AS ( SELECT id_daerah, MAX(id_jadwal) AS max_id_jadwal FROM d_jadwal WHERE tahun = ${tahun} AND is_locked = 1 GROUP BY id_daerah ),
        TotalRecord AS (
            SELECT COUNT
                ( * ) AS count 
            FROM
                d_jadwal t1
                JOIN MaxJadwal mj ON t1.id_daerah = mj.id_daerah 
                AND t1.id_jadwal = mj.max_id_jadwal 
            WHERE
                t1.tahun = $1 
            ) SELECT
            tr.count 
        FROM
            TotalRecord tr`
        const totalDataResult = await connection.select(totalDataQuery, value, daerah);

        const queryData = result.rows;
        const totalData = parseInt(totalDataResult.rows[0].count);
        const totalPages = Math.ceil(totalData / pageSize);

        const custom = queryData.map((item) => {
            if (item.is_locked === 1) {
                item.is_locked = 'Selesai dan Dikunci';
            }
            return {
                idDaerah: item.id_daerah,
                namaDaerah: item.nama_daerah,
                idJadwal: item.id_jadwal,
                namaJadwal: item.nama_sub_tahap,
                idTahap: item.id_tahap,
                namaTahap: item.nama_tahap,
                tahun: item.tahun,
                tanggalSelesai: item.waktu_selesai,
                status: item.is_locked
            }
        });
        const customResponse = {
            message: 'List Jadwal',
            code: 200,
            data: custom,
            currentPage: pageNumber,
            totalPages: totalPages,
            totalData: totalData,
            pageSize: pageSize,
        }
        // cache.set(cacheKey, customResponse, 86400);
        // const cacheStats = cache.getStats();
        // console.log(cacheStats);
        return res.status(200).json(customResponse);

    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};

module.exports = { jadwal };
