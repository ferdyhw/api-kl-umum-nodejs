const rka = require('./rkaRincian');
const jadwal = require('./jadwal');
// const evaluasiUla = require('./evaluasiToUla');
const jadwalDaerah = require('./jadwalDaerah');
const listJadwalDaerah = require('./listJadwalDaerah');


module.exports = {
    rka,
    jadwal,
    // evaluasiUla,
    jadwalDaerah,
    listJadwalDaerah
}