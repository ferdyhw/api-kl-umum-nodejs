const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const compression = require('compression')
require('dotenv').config();

const {
  USERNAME,
  PASSWORD,
} = process.env;

const auth = require('http-auth');
//Monitoring Service Auth 
const basic = auth.basic({ realm: 'Monitor Area' }, function (user, pass, callback) {
  callback(user === USERNAME && pass === PASSWORD);
});

const indexRouter = require('./routes/index');

const app = express();

const statusMonitor = require('express-status-monitor')({
  title: 'Status Service Struktur Organisasi',
  theme: '../../../../../monitor.css'
});

app.use(compression());

app.use(statusMonitor.middleware);
app.get('/monitoring', basic.check(statusMonitor.pageRoute));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

app.use('/kl-umum', indexRouter);

module.exports = app;
