const { Pool } = require('pg');
require('dotenv').config();

const {
    DB_HOST,
    DB_USER,
    DB_PASS,
    DB_PORT,
} = process.env;

// Objek untuk menyimpan koneksi pool
const poolMap = {};

exports.connect = (daerah) => {
    if (!daerah) {
        throw new Error('Daerah harus disediakan.');
    }

    // Periksa apakah koneksi untuk daerah ini sudah ada
    if (!poolMap[daerah]) {
        const connection = `sipd_anggaran_${daerah}`;
        poolMap[daerah] = new Pool({
            host: DB_HOST,
            port: DB_PORT,
            user: DB_USER,
            password: DB_PASS,
            database: connection,
            max: 20,
            idleTimeoutMillis: 300000,
            connectionTimeoutMillis: 200000,
        });
    }
};

exports.select = async (sql, data, daerah) => {
    if (!daerah) {
        throw new Error('Daerah harus disediakan.');
    }

    if (!poolMap[daerah]) {
        throw new Error(`Database pool for daerah ${daerah} is not initialized.`);
    }

    const client = await poolMap[daerah].connect();

    try {
        const result = await client.query(sql, data);
        return result;
    } catch (error) {
        throw error;
    } finally {
        client.release(); // Selalu melepaskan klien, bahkan jika terjadi kesalahan
    }
};


exports.end = (daerah) => {
    if (poolMap[daerah]) {
        poolMap[daerah].end(); // Hentikan pool jika sudah tidak diperlukan
        delete poolMap[daerah]; // Hapus koneksi pool dari objek poolMap
    }
};
