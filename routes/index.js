const express = require('express');
const router = express.Router();

const { rka, jadwal, jadwalDaerah, listJadwalDaerah } = require('../service');

router.post('/rincian', rka.rkaRincian);
router.post('/jadwal', jadwal.jadwal);
router.post('/jadwal-daerah', jadwalDaerah.jadwalDaerah);
router.post('/list-jadwal-daerah', listJadwalDaerah.listJadwalDaerah);
// router.get('/evaluasi-ula/vDokEvalRAPBD', evaluasiUla.EvaluasiUla);
module.exports = router;